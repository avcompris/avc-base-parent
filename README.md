# About avc-base-parent

This is the root project for Avantage Compris' public source.

Projects that inherit from this one include:

  * [avc-commons-parent](https://gitlab.com/avcompris/avc-commons-parent/)
  * [avc-base-testutil](https://gitlab.com/avcompris/avc-base-testutil/)
  * [avc-base-testutil-ut](https://gitlab.com/avcompris/avc-base-testutil-ut/)

This is the project home page, hosted on GitLab.

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-base-parent/)


